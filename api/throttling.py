from rest_framework.throttling import UserRateThrottle


class RitikRateThrottle(UserRateThrottle):
    scope = 'ritik'
